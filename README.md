# Progetto del corso di Programmazione 2

## Docente: 

Maurizio Atzori

## Studenti:

48974 Frongia Nicola

48978 Lepori Massimiliano

48985 Garau Nicola

## Di seguito le istruzioni per l'utilizzo:

**E' necessario solamente aprire il foglio di calcolo presente nella directory principale, tuttavia se si vuole compilare il progetto, creare il jar con tutti i file utilizzati o creare la documentazione javadoc, seguire i seguenti comandi:**

**Compilazione**

	Per prima cosa spostarsi nella root, in seguito digitare i seguenti comandi:
	
	cd java

	javac -classpath ../libs it/unica/pr2/progetto2015/interfacce/SheetFunction.java it/unica/pr2/progetto2015/g48974_48978_48985/*.java

**Creazione del jar**

	Per prima cosa spostarsi nella root, in seguito digitare i seguenti comandi: 

	cd java

	jar -cf ../build/progetto2015.jar it/* 

	cd ../libs

	jar -uf ../build/progetto2015.jar twitter4j/*

**Creazione del javadoc**

	Per prima cosa spostarsi nella root, in seguito digitare i seguenti comandi:

	javadoc -d javadoc/ java/it/unica/pr2/progetto2015/interfacce/*.java java/it/unica/pr2/progetto2015/g48974_48978_48985/*.java

**Il comandone per fare tutto:**

	Per prima cosa spostarsi nella root, in seguito digitare il seguente comando:

	cd java && javac -classpath ../libs it/unica/pr2/progetto2015/interfacce/SheetFunction.java it/unica/pr2/progetto2015/g48974_48978_48985/*.java && jar -cf ../build/progetto2015.jar it/* && cd ../libs && jar -uf ../build/progetto2015.jar twitter4j/* && cd .. && javadoc -d javadoc/ java/it/unica/pr2/progetto2015/interfacce/*.java java/it/unica/pr2/progetto2015/g48974_48978_48985/*.java

