package it.unica.pr2.progetto2015.g48974_48978_48985;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import java.util.ArrayList;

/**
 *	@author 48974 Frongia Nicola 
 *	@author 48978 Lepori Massimiliano 
 *	@author 48985 Garau Nicola
 *	@version 1.0, June 2015
 *	La classe Semplice contiene tre metodi e permette di implementare una funzione simile alla funzione ANNULLA.SPAZI presente in LibreOffice.
 *	La funzione si occupa di rimuovere tutti gli spazi superflui presenti in una stringa.
 *	La classe implementa l'interfaccia SheetFunction, della quale implementa i metodi "execute", "getCategory", "getHelp" e "getName".
 */

public class Semplice implements SheetFunction{

	private String category=("Testo");
	private String help=("ANNULLA.SPAZI Rimuove gli spazi da una stringa, lasciando un singolo spazio tra le parole. Sintassi ANNULLA.SPAZI(\"Testo\") Testo indica il testo in cui gli spazi devono essere rimossi. Esempio =ANNULLA.SPAZI(\" ciao mondo \") restituisce ciao mondo senza spazi davanti e dietro e con un singolo spazio tra le parole.");
	private String name=("ANNULLA.SPAZI");
	
	/**
	 *	Costruttore della classe Semplice, non prende in ingresso alcun parametro.
	 */
	public Semplice(){}
	
	/**
	 *	Il metodo execute permette di rimuovere tutti gli spazi superflui da una stringa passata in ingresso e restituire come output la stringa ottenuta.
	 *	@param args Una lista di parametri in ingresso. In questo caso semplicemente una stringa.
	 *	@return Un oggetto di tipo Object contenente la stringa con gli spazi superflui rimossi.
	 */
    	public Object execute(Object ... args){
		String s=new String();

		if(args[0].toString().equals("0.0")) return ""; //fix che permette di accettare anche stringhe vuote
		s=args[0].toString();
		return s.replaceAll("\\s+"," ").trim();
    	}
	
	/**
	 *	Il metodo getCategory restituisce una stringa contenente la tipologia della funzione implementata.
	 *	@return Stringa contenente la categoria della funzione implementata.
	 */
	public String getCategory(){
		return this.category;
	}

	/**
	 *	Il metodo getHelp restituisce una stringa contenente la documentazione della funzione LibreOffice implementata.
	 *	@return Stringa contenente la documentazione della funzione LibreOffice implementata.
	 */
	public String getHelp(){ 
		return this.help; }

	/**
	 *	Il metodo getName restituisce una stringa contenente il nome della funzione LibreOffice implementata.
	 *	@return Stringa contenente la documentazione della funzione LibreOffice implementata.
	 */
	public String getName(){
		return this.name;
	}
}
