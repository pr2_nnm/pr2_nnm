package it.unica.pr2.progetto2015.g48974_48978_48985;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import java.util.List;
import twitter4j.*;

/**
 *	@author 48974 Frongia Nicola 
 *	@author 48978 Lepori Massimiliano 
 *	@author 48985 Garau Nicola
 *	@version 1.0, June 2015
 *	La classe Custom contiene tre metodi e permette di implementare una funzione non presente in LibreOffice.
 *	La funzione utilizza le librerie twitter4j e per farlo si appoggia alla classe MyTweetsClass.
 *	La classe implementa l'interfaccia SheetFunction, della quale implementa i metodi "execute", "getCategory", "getHelp" e "getName".
 */

public class Custom implements SheetFunction{

	private String category=("Twitter");
	private String help=("Restituisce gli ultimi 10 tweet della timeline di un utente Twitter");
	private String name=("Custom");
	
	/**
	 *	Costruttore della classe Custom, non prende in ingresso alcun parametro.
	 */
	public Custom(){}

	/**
	 *	Il metodo execute permette di estrarre gli ultimi 10 tweet dalla timeline di un utente e restituirli come output.
	 *	@param args Una lista di parametri in ingresso. In questo caso sono cinque stringhe: nome utente più i quattro token Twitter necessari.
	 *	@return Un oggetto di tipo Object contenente un vettore di String rappresentante i tweet dell'utente.
	 */
    	public Object execute(Object ... args){
		int dim=11;
		String[] mioArray = new String[dim];
		String[] inputArray=new String[5];
		int j=0;
		for(Object o : args) {
	        	inputArray[j] = o.toString();	
			j++;
	 	}
		mioArray = (new MyTweetsClass()).getTweets(inputArray[0], inputArray[1], inputArray[2], inputArray[3], inputArray[4]);
		
		return mioArray;
    	}
	
	/**
	 *	Il metodo getCategory restituisce una stringa contenente la tipologia della funzione.
	 *	@return Stringa contenente la categoria della funzione implementata.
	 */
	public String getCategory(){
		return this.category;
	}

	/**
	 *	Il metodo getHelp restituisce una stringa contenente la documentazione della funzione.
	 *	@return Stringa contenente la documentazione della funzione LibreOffice implementata.
	 */
	public String getHelp(){ 
		return this.help; }

	/**
	 *	Il metodo getName restituisce una stringa contenente il nome della funzione.
	 *	@return Stringa contenente la documentazione della funzione LibreOffice implementata.
	 */
	public String getName(){
		return this.name;
	}
}
